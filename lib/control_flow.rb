# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.chars.select {|ch| ch != ch.downcase}.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length % 2 != 0
    return str[str.length / 2]
  else
    return str[str.length / 2 - 1] + str[str.length / 2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.each_char.count {|ch| VOWELS.include?(ch)}
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  1.upto(num).reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  new_str = ""
  arr.each do |ch|
    new_str += ch + separator
  end
  if separator == ""
    return new_str
  else
    return new_str[0...-1]
  end
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str.each_char.with_index.map {|ch, idx| idx % 2 == 0 ? ch = ch.downcase : ch = ch.upcase}.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)

  new_str = str.split.each.map {|word|
    if word.length >= 5
      word = word.reverse
    else
      word
    end
  }.join(" ")
  return new_str
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  nums = (1..n).to_a
  nums.each.map do |num|
    if num % 3 == 0
      if num % 5 == 0
        num = 'fizzbuzz'
      else
        num = 'fizz'
      end
    elsif num % 5 == 0
      num = 'buzz'
    else
      num = num
    end
  end
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reversed_arr = []
  while arr.length > 0
    reversed_arr << arr.pop
  end
  reversed_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  if num == 1
    return false
  end
  2.upto(num - 1).each do |digit|
    if num % digit == 0
      return false
    end
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors = []
  1.upto(num).each do |n|
    if num % n == 0
      factors << n
    end
  end
  return factors
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors = factors(num)
  factors.select {|n| prime?(n)}
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  if arr.select {|n| n.odd?}.count > 1
    parity = 'odd'
  else
    parity = 'even'
  end

  if parity == 'odd'
    return arr.select {|n| n.even?}[0]
  else
    return arr.select {|n| n.odd?}[0]
  end
end
